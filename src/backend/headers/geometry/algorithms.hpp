#pragma once

// Every algorithm should have a short proof or explanation of why it works

#include "convex_polygon.hpp"
#include "interval.hpp"
#include "point.hpp"
#include "project.hpp"
#include "rectangle.hpp"
#include "segment.hpp"

namespace geometry {

// ----------------------------------------------------------------------------
// Element
// ----------------------------------------------------------------------------

// This assumes that point is already collinear with the line segment.
template <Topology Top>
bool collinear_element(const Point& point, const Segment<Top>& seg) {

    if (seg.is_vertical()) {
        return element(point.y, std::get<Interval<Top>>(project_y(seg)));
    } else {
        return element(point.x, std::get<Interval<Top>>(project_x(seg)));
    }
}

inline bool element(const mpq::Rational& num, const Interval<Topology::Closed>& interval) {
    return interval.lower() <= num && num <= interval.upper();
}

inline bool element(const mpq::Rational& num, const Interval<Topology::Open>& interval) {
    return interval.lower() < num && num < interval.upper();
}

template <Topology Top>
bool element(const Point& point, const Rectangle<Top>& rectangle) {
    return element(point.x, rectangle.interval_x()) && element(point.y, rectangle.interval_y());
}

template <Topology Top>
bool element(const Point& point, const Segment<Top>& segment) {

    if (segment.side(point) != 0) {
        return false;
    }

    return collinear_element(point, segment);
}

// In theory, this algorithm could be optimized to be O(log n)
inline bool element(const Point& point, const ConvexPolygon<Topology::Closed>& polygon) {

    for (size_t i = 0; i < polygon.size(); ++i) {

        const auto& vi = polygon.vertex(i);
        const auto& vii = polygon.vertex(i + 1);

        const auto orient = Point::orientation(vi, vii, point);

        if (orient == 0) {
            // The point is collinear with the side

            // This polygon is closed, so points on the boundary
            // are inside the polygon

            // The polygon is strictly convex, so this is now
            // equivalent to if the point is inside the segment
            // (since there are no collinear sides)
            const Segment<Topology::Closed> side{vi, vii};
            return collinear_element(point, side);
        } else if (orient < 0) {
            // On the outside
            return false;
        }
        // else it has positive orientation, so continue
    }

    // p is on the same side of every edge, so it is on the inside
    return true;
}

// Element of the closure
inline bool element_or_boundary(const Point& point, const ConvexPolygon<Topology::Open>& polygon) {

    for (size_t i = 0; i < polygon.size(); ++i) {

        const auto& vi = polygon.vertex(i);
        const auto& vii = polygon.vertex(i + 1);

        const auto orient = Point::orientation(vi, vii, point);

        if (orient == 0) {
            // The point is collinear with the side

            // This polygon is closed, so points on the boundary
            // are inside the polygon

            // The polygon is strictly convex, so this is now
            // equivalent to if the point is inside the segment
            // (since there are no collinear sides)
            const Segment<Topology::Closed> side{vi, vii};
            return collinear_element(point, side);
        } else if (orient < 0) {
            // On the outside
            return false;
        }
        // else it has positive orientation, so continue
    }

    // p is on the same side of every edge, so it is on the inside
    return true;
}

inline bool element(const Point& point, const ConvexPolygon<Topology::Open>& polygon) {

    // Since the polygon has been normalized to be in a ccw orientation, the
    // point should have a ccw orientation with respect to each side
    // The polygon is also open, so being collinear with a side means not an element

    for (size_t i = 0; i < polygon.size(); ++i) {

        const auto& vi = polygon.vertex(i);
        const auto& vii = polygon.vertex(i + 1);

        const auto orient = Point::orientation(vi, vii, point);

        if (orient <= 0) {
            return false;
        }
    }

    return true;
}

// ----------------------------------------------------------------------------
// Subset
// ----------------------------------------------------------------------------

// This is identical for open and closed intervals
template <Topology Top>
bool subset(const Interval<Top>& a, const Interval<Top>& b) {
    return b.lower() <= a.lower() && a.upper() <= b.upper();
}

template <Topology Top>
bool subset(const Rectangle<Top>& a, const Rectangle<Top>& b) {
    return subset(a.interval_x(), b.interval_x()) && subset(a.interval_y(), b.interval_y());
}

template <Topology Top>
bool subset(const ConvexPolygon<Topology::Closed>& polygon, const Rectangle<Top>& rectangle) {

    for (const auto& vertex : polygon) {
        if (!element(vertex, rectangle)) {
            return false;
        }
    }

    return true;
}

inline bool subset(const ConvexPolygon<Topology::Open>& polygon, const Rectangle<Topology::Closed>& rectangle) {

    for (const auto& vertex : polygon) {
        if (!element(vertex, rectangle)) {
            return false;
        }
    }

    return true;
}

// A closed rectangle is a subset of a convex polygon (open or closed) iff each vertex of the first is in the other
template <Topology Top>
bool subset(const Rectangle<Topology::Closed>& rectangle, const ConvexPolygon<Top>& polygon) {

    const auto ll = rectangle.lower_left();
    const auto lr = rectangle.lower_right();

    const auto ul = rectangle.upper_left();
    const auto ur = rectangle.upper_right();

    return element(ll, polygon) && element(lr, polygon) && element(ul, polygon) && element(ur, polygon);
}

// ----------------------------------------------------------------------------
// Intersects
// ----------------------------------------------------------------------------

// Using separating axes, it is easier to define a disjoint function, and then define intersects
// in terms of that. I think this will work for whatever types we have (open or closed)

inline bool disjoint(const Interval<Topology::Open>& a, const Interval<Topology::Open>& b) {
    return b.upper() <= a.lower() || a.upper() <= b.lower();
}

inline bool disjoint(const Interval<Topology::Open>& a, const Interval<Topology::Closed>& b) {
    return b.upper() <= a.lower() || a.upper() <= b.lower();
}

inline bool disjoint(const Interval<Topology::Closed>& a, const Interval<Topology::Open>& b) {
    return b.upper() <= a.lower() || a.upper() <= b.lower();
}

inline bool disjoint(const Interval<Topology::Closed>& a, const Interval<Topology::Closed>& b) {
    return b.upper() < a.lower() || a.upper() < b.lower();
}

template <Topology Top1, Topology Top2>
struct Disjoint final {

    const Interval<Top1>& a;

    explicit Disjoint(const Interval<Top1>& a_)
        : a{a_} {}

    bool operator()(const mpq::Rational& p) const {
        return !element(p, a);
    }

    bool operator()(const Interval<Top2>& b) const {
        return disjoint(a, b);
    }
};

template <Topology Top1, Topology Top2>
bool disjoint(const Interval<Top1>& a, const std::variant<Interval<Top2>>& b) {

    return std::visit(Disjoint<Top1, Top2>{a}, b);
}

template <Topology Top1, Topology Top2>
bool disjoint(const std::variant<Interval<Top1>>& b, const Interval<Top2>& a) {

    return std::visit(Disjoint<Top2, Top1>{a}, b);
}

// Our segments, rectangles, and polygons are always convex (open or closed)

// The basic idea is this: suppose two convex sets A and B intersect at a point x. Then, along
// any axis that you project A and B, their projections will intersect, because they have
// a point in common. Taking the contrapositive, if a single projection does not intersect
// (a single projection is disjoint) then they do not intersect.
// For our shapes, the idea is that it is sufficient to project only along certain special axes.
// These are the only axes you need to check. I think this is true.
// If disjoint on one of our axes, then disjoint. (proved this above)
// If intersect on all axes, then intersect.

// Hyperplane separation theorem
// https://en.wikipedia.org/wiki/Hyperplane_separation_theorem
// Separation Theorem I - if disjoint => such a separation exists. And clearly, if such a separation exists, then they
// are disjoint. This is true for all compact sets.
// Separation Theorem II - sameses for open sets. However, this does not apply to open segments, since they aren't strictly
// open.

template <Topology Top1, Topology Top2, template <Topology> typename Shape>
bool separating_axis(const Segment<Top1>& segment, const Shape<Top2>& shape) {

    Point axis{};

    // vec = segment.end() - segment.start()
    // axis = {-vec.y, vec.x}
    mpq::sub(axis.x, segment.start().y, segment.end().y);
    mpq::sub(axis.y, segment.end().x, segment.start().x);

    const auto line_seg_project = project(segment, axis);
    const auto shape_project = project(shape, axis);

    return disjoint(line_seg_project, shape_project);
}

// Project this and the shape along the sides of the rectangle
template < Topology Top1, Topology Top2, template <Topology> typename Shape>
bool separating_axis(const Rectangle<Top1>& rectangle, const Shape<Top2>& shape) {

    // The axes for the rectangle are just the x and y axes

    const auto shape_project_x = project_x(shape);
    const auto shape_project_y = project_y(shape);

    // The projections of this rectangle along the x and y
    // axes are just interval_x() and interval_y()

    // If either of the intervals are disjoint, then
    // there is a separating axis

    return disjoint(rectangle.interval_x(), shape_project_x) || disjoint(rectangle.interval_y(), shape_project_y);
}

// The shape just needs to have a project() method
template <Topology Top1, Topology Top2, template <Topology> typename Shape>
bool separating_axis(const ConvexPolygon<Top1>& polygon, const Shape<Top2>& shape) {

    Point axis{};

    for (size_t i = 0; i < polygon.size(); ++i) {
        const auto& p0 = polygon.vertex(i);
        const auto& p1 = polygon.vertex(i + 1);

        // The order and perpendicular vector are rather arbitrary
        // as long as they lie on the same line

        // edge = p1 - p0, vector from p0 to p1
        // axis = rotate 90 CCW edge, perpendicular to the edge

        // axis.x = p0.y - p1.y
        // axis.y = p1.x - p0.x

        mpq::sub(axis.x, p0.y, p1.y);
        mpq::sub(axis.y, p1.x, p0.x);

        const auto poly_project = project(polygon, axis);
        const auto shape_project = project(shape, axis);

        if (disjoint(poly_project, shape_project)) {
            return true;
        }
    }

    return false;
}

template <typename Shape1, typename Shape2>
bool disjoint(const Shape1& shape1, const Shape2& shape2) {
    return separating_axis(shape1, shape2) || separating_axis(shape2, shape1);
}

template <typename T, typename S>
bool intersects(const T& a, const S& b) {
    return !disjoint(a, b);
}
}
