package billiards.viewer;

import billiards.codeseq.CodePair;
import billiards.wrapper.CodeInfo;
import billiards.codeseq.CodeSequence;
import billiards.codeseq.InitialAngles;
import billiards.codeseq.TriplePair;
import billiards.geometry.ConvexPolygon;
import billiards.geometry.Rectangle;
import billiards.geometry.Point;
import billiards.math.CoverSquare;
import billiards.math.XYZ;

import com.google.common.base.Splitter;

import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.impl.tuple.Tuples;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.api.list.primitive.IntList;
import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.impl.list.mutable.FastList;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class Cover {

    // TODO change this
    public static InitialAngles parseInitialAngles(final String str) {
        switch (str) {
            case "xy":
                return new InitialAngles(XYZ.X, XYZ.Y);
            case "xz":
                return new InitialAngles(XYZ.X, XYZ.Z);
            case "yx":
                return new InitialAngles(XYZ.Y, XYZ.X);
            case "yz":
                return new InitialAngles(XYZ.Y, XYZ.Z);
            case "zx":
                return new InitialAngles(XYZ.Z, XYZ.X);
            case "zy":
                return new InitialAngles(XYZ.Z, XYZ.Y);
            default:
                throw new RuntimeException("unable to parse initial angles: " + str);
        }
    }

    public static ConvexPolygon parsePolygon(final String string) {

        final MutableList<Point> vertices = new FastList<>();

        final Iterable<String> lines = Splitter.on('\n').split(string);

        for (final String line : lines) {

            final List<String> coords = Splitter.on(' ').splitToList(line);

            final double x = CodeInfo.rationalToRadians(coords.get(0));
            final double y = CodeInfo.rationalToRadians(coords.get(1));

            final Point vertex = Point.create(x, y);
            vertices.add(vertex);
        }

        return ConvexPolygon.create(vertices.toImmutable());
    }

    public static Rectangle parseRectangle(final String string) {

        final List<String> coords = Splitter.on(' ').splitToList(string);

        final double xMin = CodeInfo.rationalToRadians(coords.get(0));
        final double xMax = CodeInfo.rationalToRadians(coords.get(1));

        final double yMin = CodeInfo.rationalToRadians(coords.get(2));
        final double yMax = CodeInfo.rationalToRadians(coords.get(3));

        return Rectangle.create(xMin, xMax, yMin, yMax);
    }

    public static List<CodePair> parseStables(final String string) {

        final MutableList<CodePair> stablePairs = new FastList<>();

        final List<String> lines = Splitter.on('\n').omitEmptyStrings().splitToList(string);

        for (int i = 0; i < lines.size(); ++i) {

            final String line = lines.get(i).replace(":", ",");

            final List<String> comps = Splitter.on(',').trimResults().splitToList(line);

            final int fileIndex = Integer.parseInt(comps.get(0));

            if (fileIndex != i) {
                throw new RuntimeException("mismatched indices: " + fileIndex + ", " + i);
            }

            final String codeString = comps.get(1);

            final IntList codeList = Utils.splitString(codeString).get();

            final CodeSequence codeSeq = new CodeSequence(codeList);

            final String initialAnglesString = comps.get(2);

            final InitialAngles initialAngles = parseInitialAngles(initialAnglesString);

            final CodePair stablePair = new CodePair(codeSeq, initialAngles);

            stablePairs.add(stablePair);
        }

        return stablePairs;
    }

    public static List<TriplePair> parseTriples(final String string) {

        final MutableList<TriplePair> triplePairs = new FastList<>();

        final List<String> lines = Splitter.on('\n').omitEmptyStrings().splitToList(string);

        for (int i = 0; i < lines.size(); ++i) {

            final String line = lines.get(i).replace(":", ",").replace(";", ",");

            final List<String> comps = Splitter.on(',').trimResults().splitToList(line);

            final int fileIndex = Integer.parseInt(comps.get(0));

            if (fileIndex != i) {
                throw new RuntimeException("mismatched indices: " + fileIndex + ", " + i);
            }

            final String stableNegString = comps.get(1);
            final String stableNegAnglesString = comps.get(2);

            final String unstableString = comps.get(3);
            final String unstableAnglesString = comps.get(4);

            final String stablePosString = comps.get(5);
            final String stablePosAnglesString = comps.get(6);

            final IntList stableNegList = Utils.splitString(stableNegString).get();
            final IntList unstableList = Utils.splitString(unstableString).get();
            final IntList stablePosList = Utils.splitString(stablePosString).get();

            final CodeSequence stableNeg = new CodeSequence(stableNegList);
            final CodeSequence unstable = new CodeSequence(unstableList);
            final CodeSequence stablePos = new CodeSequence(stablePosList);

            final InitialAngles stableNegAngles = parseInitialAngles(stableNegAnglesString);
            final InitialAngles unstableAngles = parseInitialAngles(unstableAnglesString);
            final InitialAngles stablePosAngles = parseInitialAngles(stablePosAnglesString);

            final CodePair stableNegPair = new CodePair(stableNeg, stableNegAngles);
            final CodePair unstablePair = new CodePair(unstable, unstableAngles);
            final CodePair stablePosPair = new CodePair(stablePos, stablePosAngles);

            final TriplePair triplePair = new TriplePair(stableNegPair, unstablePair, stablePosPair);

            triplePairs.add(triplePair);
        }

        return triplePairs;
    }

    private static Rectangle[] subdivide(final Rectangle rect) {

        // Split the rect into four quarters

        final Point center = rect.center();

        final Rectangle upperLeft = Rectangle.create(rect.intervalX.min, center.x, center.y, rect.intervalY.max);

        final Rectangle upperRight = Rectangle.create(center.x, rect.intervalX.max, center.y, rect.intervalY.max);

        final Rectangle lowerLeft = Rectangle.create(rect.intervalX.min, center.x, rect.intervalY.min, center.y);

        final Rectangle lowerRight = Rectangle.create(center.x, rect.intervalX.max, rect.intervalY.min, center.y);

        // This is UL, UR, LL, LR
        return new Rectangle[] {upperLeft, upperRight, lowerLeft, lowerRight};
    }

    private static void parseCover(final Iterator<String> tokens, final CoverSquare square,
    		final List<CodePair> stables, final List<TriplePair> triples,
    		final Map<CoverSquare, CodePair> stableCover, final Map<CoverSquare, TriplePair> tripleCover) {

        final String token = tokens.next();

        if (token.equals("E")) {
            // Nothing to do
        } else if (token.equals("S")) {
            final String str = tokens.next();
            final int index = Integer.parseInt(str);

            final CodePair stable = stables.get(index);
            stableCover.put(square, stable);
        } else if (token.equals("T")) {
            final String str = tokens.next();
            final int index = Integer.parseInt(str);

            final TriplePair triple = triples.get(index);
            tripleCover.put(square, triple);
        } else if (token.equals("D")) {

            final CoverSquare[] quarters = square.subdivide();

            for (final CoverSquare quarter : quarters) {
                parseCover(tokens, quarter, stables, triples, stableCover, tripleCover);
            }

        } else {
            throw new RuntimeException("unknown cover token: " + token);
        }
    }

    public static Pair<Map<CoverSquare, CodePair>, Map<CoverSquare, TriplePair>> parseCover(
    		final String coverString, final CoverSquare square, final List<CodePair> stables, final List<TriplePair> triples) {

        final Iterator<String> tokens = Splitter.on(' ').split(coverString).iterator();

        final MutableMap<CoverSquare, CodePair> stableCover = new UnifiedMap<>();
        final MutableMap<CoverSquare, TriplePair> tripleCover = new UnifiedMap<>();

        parseCover(tokens, square, stables, triples, stableCover, tripleCover);

        if (tokens.hasNext()) {
            throw new RuntimeException("unused tokens when parsing a cover");
        }

        return Tuples.pair(stableCover, tripleCover);
    }
}
