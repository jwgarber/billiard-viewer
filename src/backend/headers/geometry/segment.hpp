#pragma once

#include <ostream>   // std::ostream
#include <stdexcept> // std::runtime_error

#include "point.hpp"
#include "topology.hpp"

namespace geometry {

// A directed line segment from start -> end
// Various algorithms require that this be directed. Plus, having it undirected is just weird

template <Topology Top>
class Segment final {

  private:
    Point start_;
    Point end_;

    void check() const {
        if (start_ == end_) {
            throw std::runtime_error("degenerate Segment: start and end are equal");
        }
    }

  public:
    Segment(Point start, Point end)
        : start_{std::move(start)}, end_{std::move(end)} {
        check();
    }

    const Point& start() const {
        return start_;
    }

    const Point& end() const {
        return end_;
    }

    bool is_horizontal() const {
        return start_.y == end_.y;
    }

    bool is_vertical() const {
        return start_.x == end_.x;
    }

    Point midpoint() const {
        // return (start_ + end_) / 2;

        Point m{};

        mpq::add(m.x, start_.x, end_.x);
        mpq::add(m.y, start_.y, end_.y);

        mpq::div_2exp(m.x, m.x, 1);
        mpq::div_2exp(m.y, m.y, 1);

        return m;
    }

    /*
    // The vector from start_ -> end_
    Point vector() const {
        return end_ - start_;
    }
    */

    // This is the same as the orientation of the points start -> end -> p
    int side(const Point& point) const {
        return Point::orientation(start_, end_, point);
    }

    // ------------------------------------------------------------------------
    // Operators
    // ------------------------------------------------------------------------

    friend bool operator==(const Segment<Top>& lhs, const Segment<Top>& rhs) {
        return std::tie(lhs.start_, lhs.end_) == std::tie(rhs.start_, rhs.end_);
    }

    friend bool operator!=(const Segment<Top>& lhs, const Segment<Top>& rhs) {
        return !(lhs == rhs);
    }

    friend bool operator<(const Segment<Top>& lhs, const Segment<Top>& rhs) {
        return std::tie(lhs.start_, lhs.end_) < std::tie(rhs.start_, rhs.end_);
    }

    friend bool operator>(const Segment<Top>& lhs, const Segment<Top>& rhs) {
        return rhs < lhs;
    }

    friend bool operator<=(const Segment<Top>& lhs, const Segment<Top>& rhs) {
        return !(lhs > rhs);
    }

    friend bool operator>=(const Segment<Top>& lhs, const Segment<Top>& rhs) {
        return rhs <= lhs;
    }

    friend std::ostream& operator<<(std::ostream& os, const Segment<Top>& seg) {
        return os << seg.start_ << " -> " << seg.end_;
    }
};
}
