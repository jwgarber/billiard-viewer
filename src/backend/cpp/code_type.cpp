#include "code_type.hpp"
#include "util.hpp"

std::ostream& operator<<(std::ostream& os, const CodeType code_type) {

    switch (code_type) {
    case CodeType::OSO:
        return os << "OSO";
    case CodeType::ESP:
        return os << "ESP";
    case CodeType::ESO:
        return os << "ESO";
    case CodeType::EUP:
        return os << "EUP";
    case CodeType::EUO:
        return os << "EUO";
    }

    throw std::runtime_error(invalid_enum_value("CodeType", code_type));
}

bool is_stable(const CodeType code_type) {

    switch (code_type) {
    case CodeType::OSO:
    case CodeType::ESP:
    case CodeType::ESO:
        return true;
    case CodeType::EUP:
    case CodeType::EUO:
        return false;
    }

    throw std::runtime_error(invalid_enum_value("CodeType", code_type));
}
