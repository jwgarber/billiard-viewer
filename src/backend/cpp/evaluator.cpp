#include "evaluator.hpp"

struct FreeCache {
    ~FreeCache() {
        mpfr::free_cache();
    }
};

// This will free the cache once each thread exits.
static const thread_local FreeCache free_cache{};

// The semantics of MPFR is that it calculates the correct answer to "infinite" precision,
// and then rounds as requested to the precise number of bits. In particular, rounding will
// always be within 1 ULP

template <template <typename> typename Trig>
void eval_trig_helper(mpfr::Float& term, const Coeff64 coeff, const mpq::Rational& frac,
                      const mpfr::Float& half_pi_d, const mpfr::Float& half_pi_u);

template <>
void eval_trig_helper<Sin>(mpfr::Float& term, const Coeff64 coeff, const mpq::Rational& frac,
                           const mpfr::Float& half_pi_d, const mpfr::Float& half_pi_u) {

    // sin is increasing on [0, pi\2)

    if (coeff > 0) {

        // Round down the argument
        mpfr::mul(term, half_pi_d, frac, mpfr::Round::Down);

        // In the directed rounding modes, MPFR will always round each result
        // to the closest representable float in the given direction.
        // Since MPFR can exactly represent 0, the above multiplication will
        // then always stay within the first quadrant (and never round down
        // to the lower one).

        // Round down the trig function
        mpfr::sin(term, term, mpfr::Round::Down);

    } else if (coeff < 0) {

        // Round up the argument
        mpfr::mul(term, half_pi_u, frac, mpfr::Round::Up);

        // This case is not as simple as the above one.
        // In the above multiplication, recall that 0 <= frac < 1. Ideally,
        // this would imply that
        //
        //     term = frac * half_pi_u < pi/2
        //
        // However, when doing the above multiplication, it may be possible that
        // frac is so close to 1 that the term is rounded up to half_pi_u.
        // In this case, the term is now in the next quadrant, and we
        // loose monotonicity. To avoid this, we check if the term
        // is greater than half_pi_d, and if so bump it up to sin(pi/2) = 1,
        // which is the most pessimistic upper bound in this case.
        if (term > half_pi_d) {
            // sin(pi/2) = 1
            mpfr::set(term, 1, mpfr::Round::Up);
        } else {
            // Else we can just find the sin like normal
            mpfr::sin(term, term, mpfr::Round::Up);
        }

    } else {
        throw std::runtime_error("eval_trig<Sin>: zero coeff");
    }

    // In the future it might be interesting to try FMA, but for now we'll
    // keep it simple.
    mpfr::mul(term, term, coeff, mpfr::Round::Down);
}

template <>
void eval_trig_helper<Cos>(mpfr::Float& term, const Coeff64 coeff, const mpq::Rational& frac,
                           const mpfr::Float& half_pi_d, const mpfr::Float& half_pi_u) {

    // cos is decreasing on [0, pi/2)

    if (coeff > 0) {

        // Round up the argument
        mpfr::mul(term, half_pi_u, frac, mpfr::Round::Up);

        // Same as with the sin case
        if (term > half_pi_d) {
            // cos(pi/2) = 0
            mpfr::set_zero(term, 0);
        } else {
            // Else we can just find the cos like normal
            mpfr::cos(term, term, mpfr::Round::Down);
        }

    } else if (coeff < 0) {

        // Round down the argument
        mpfr::mul(term, half_pi_d, frac, mpfr::Round::Down);

        // Round up the trig function
        mpfr::cos(term, term, mpfr::Round::Up);

    } else {
        throw std::runtime_error("eval_trig<Cos>: zero coeff");
    }

    mpfr::mul(term, term, coeff, mpfr::Round::Down);
}

template <template <typename> typename Trig>
void eval_trig(mpfr::Float& term, const Coeff64 coeff, const mpq::Rational& frac, const unsigned long quad,
               const mpfr::Float& half_pi_d, const mpfr::Float& half_pi_u);

template <>
void eval_trig<Sin>(mpfr::Float& term, const Coeff64 coeff, const mpq::Rational& frac, const unsigned long quad,
                    const mpfr::Float& half_pi_d, const mpfr::Float& half_pi_u) {

    if (quad == 0) {
        // sin(x + 0) = sin(x)
        eval_trig_helper<Sin>(term, coeff, frac, half_pi_d, half_pi_u);

    } else if (quad == 1) {
        // sin(x + pi/2) = cos(x)
        eval_trig_helper<Cos>(term, coeff, frac, half_pi_d, half_pi_u);

    } else if (quad == 2) {
        // sin(x + pi) = -sin(x)
        eval_trig_helper<Sin>(term, -coeff, frac, half_pi_d, half_pi_u);

    } else if (quad == 3) {
        // sin(x + 3pi/2) = -cos(x)
        eval_trig_helper<Cos>(term, -coeff, frac, half_pi_d, half_pi_u);

    } else {
        std::ostringstream oss{};
        oss << "eval_trig<Sin>: unknown quadrant " << quad;
        throw std::runtime_error(oss.str());
    }
}

template <>
void eval_trig<Cos>(mpfr::Float& term, const Coeff64 coeff, const mpq::Rational& frac, const unsigned long quad,
                    const mpfr::Float& half_pi_d, const mpfr::Float& half_pi_u) {

    if (quad == 0) {
        // cos(x + 0) = cos(x)
        eval_trig_helper<Cos>(term, coeff, frac, half_pi_d, half_pi_u);

    } else if (quad == 1) {
        // cos(x + pi/2) = -sin(x)
        eval_trig_helper<Sin>(term, -coeff, frac, half_pi_d, half_pi_u);

    } else if (quad == 2) {
        // cos(x + pi) = -cos(x)
        eval_trig_helper<Cos>(term, -coeff, frac, half_pi_d, half_pi_u);

    } else if (quad == 3) {
        // cos(x + 3pi/2) = sin(x)
        eval_trig_helper<Sin>(term, coeff, frac, half_pi_d, half_pi_u);

    } else {
        std::ostringstream oss{};
        oss << "eval_trig<Cos>: unknown quadrant " << quad;
        throw std::runtime_error(oss.str());
    }
}

template <template <typename> typename Trig>
bool Evaluator::is_positive(const EqVec<Trig>& eq, const Coeff64 bx, const Coeff64 by,
                            const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry) {

    mpfr::clear_flags();

    mpfr::set_zero(sum, 0);

    for (const auto& kv : eq) {

        // t_coeff * trig(x_coeff * center.x * pi/2 + y_coeff * center.y * pi/2)

        const auto x_coeff = kv.first.arg.coeff(XY::X);
        const auto y_coeff = kv.first.arg.coeff(XY::Y);
        const auto t_coeff = kv.second;

        mpq::set(xq, x_coeff);
        mpq::set(yq, y_coeff);

        mpq::mul(xq, xq, center.x);
        mpq::mul(yq, yq, center.y);

        mpq::add(argq, xq, yq);

        mpz_fdiv_qr(quot.data(), rem.data(), mpq_numref(argq.data()), mpq_denref(argq.data()));

        // Reuse xq, yq, argq

        mpq::set(xq, rem);
        mpq_set_z(yq.data(), mpq_denref(argq.data()));

        // This is the remainder fraction. 0 <= argq < 1
        mpq::div(argq, xq, yq);

        // The return value of this function is the remainder
        const auto quad = mpz::fdiv(quot, 4);

        // We can ignore the quotient part of the above remainder, since
        // sin(x + 2pi) = sin(x)
        // cos(x + 2pi) = cos(x)

        eval_trig<Trig>(term, t_coeff, argq, quad, half_pi_d, half_pi_u);

        mpfr::add(sum, sum, term, mpfr::Round::Down);
    }

    // Reuse xq, yq, argq
    mpq::set(xq, bx);
    mpq::set(yq, by);

    mpq::mul(xq, xq, rx);
    mpq::mul(yq, yq, ry);

    mpq::add(argq, xq, yq);

    // Reuse term
    mpfr::mul(term, half_pi_u, argq, mpfr::Round::Up);

    const bool is_pos = sum > term;

    // Now we check for errors.
    // We don't check the INEXACT flag, because that one will be set when evaluating the trig functions
#if 0
    const auto mask = mpfr::Flags::Underflow | mpfr::Flags::Overflow | mpfr::Flags::NaN | mpfr::Flags::Erange;

    const auto flags = mpfr::flags_test(mask);

    if (!mpfr::flags_empty(flags)) {

        std::ostringstream oss{};
        oss << "error: flags raised in calculation of " << eq
            << " at point " << center
            << " with radius " << radius << '\n';

        if (flags & mpfr::Flags::Underflow) {
            oss << "underflow\n";
        }

        if (flags & mpfr::Flags::Overflow) {
            oss << "overflow\n";
        }

        //if (flags & MPFR_FLAGS_DIVBY0) {
            //oss << "divby0\n";
        //}

        if (flags & mpfr::Flags::NaN) {
            oss << "nan\n";
        }

        if (flags & mpfr::Flags::Erange) {
            oss << "erange\n";
        }

        throw std::runtime_error(oss.str());
    }
#endif

    const bool err = mpfr::underflow_flag() || mpfr::overflow_flag() || mpfr::nan_flag() || mpfr::erange_flag();

    if (err) {

        std::ostringstream oss{};
        oss << "error: flags raised in calculation of " << eq << '\n'
            << " at point " << center << '\n'
            << " with radius " << rx << ", " << ry << '\n';

        if (mpfr::underflow_flag()) {
            oss << "underflow\n";
        }

        if (mpfr::overflow_flag()) {
            oss << "overflow\n";
        }

        if (mpfr::nan_flag()) {
            oss << "nan\n";
        }

        if (mpfr::erange_flag()) {
            oss << "erange\n";
        }

        throw std::runtime_error(oss.str());
    }

    return is_pos;
}

template bool Evaluator::is_positive(const EqVec<Sin>& eq, const Coeff64 bx, const Coeff64 by,
                                     const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);
template bool Evaluator::is_positive(const EqVec<Cos>& eq, const Coeff64 bx, const Coeff64 by,
                                     const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);

template <template <typename> typename Trig>
bool Evaluator::is_positive(const EqMap<Trig>& eq, const Coeff64 bx, const Coeff64 by,
                            const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry) {

    mpfr::clear_flags();

    mpfr::set_zero(sum, 0);

    for (const auto& kv : eq) {

        // t_coeff * trig(x_coeff * center.x * pi/2 + y_coeff * center.y * pi/2)

        const auto x_coeff = kv.first.arg.coeff(XY::X);
        const auto y_coeff = kv.first.arg.coeff(XY::Y);
        const auto t_coeff = kv.second;

        mpq::set(xq, x_coeff);
        mpq::set(yq, y_coeff);

        mpq::mul(xq, xq, center.x);
        mpq::mul(yq, yq, center.y);

        mpq::add(argq, xq, yq);

        mpz_fdiv_qr(quot.data(), rem.data(), mpq_numref(argq.data()), mpq_denref(argq.data()));

        // Reuse xq, yq, argq

        mpq::set(xq, rem);
        mpq_set_z(yq.data(), mpq_denref(argq.data()));

        // This is the remainder fraction. 0 <= argq < 1
        mpq::div(argq, xq, yq);

        // The return value of this function is the remainder
        const auto quad = mpz::fdiv(quot, 4);

        // We can ignore the quotient part of the above remainder, since
        // sin(x + 2pi) = sin(x)
        // cos(x + 2pi) = cos(x)

        eval_trig<Trig>(term, t_coeff, argq, quad, half_pi_d, half_pi_u);

        mpfr::add(sum, sum, term, mpfr::Round::Down);
    }

    // Reuse xq, yq, argq
    mpq::set(xq, bx);
    mpq::set(yq, by);

    mpq::mul(xq, xq, rx);
    mpq::mul(yq, yq, ry);

    mpq::add(argq, xq, yq);

    // Reuse term
    mpfr::mul(term, half_pi_u, argq, mpfr::Round::Up);

    const auto is_pos = sum > term;

    // Now we check for errors.
    // We don't check the INEXACT flag, because that one will be set when evaluating the trig functions
#if 0
    const auto mask = MPFR_FLAGS_UNDERFLOW | MPFR_FLAGS_OVERFLOW | MPFR_FLAGS_NAN | MPFR_FLAGS_ERANGE;

    const auto flags = mpfr_flags_test(mask);

    if (flags != 0) {

        std::ostringstream oss{};
        oss << "error: flags raised in calculation of " << eq
            << " at point " << center
            << " with radius " << radius << '\n';

        if (flags & MPFR_FLAGS_UNDERFLOW) {
            oss << "underflow\n";
        }

        if (flags & MPFR_FLAGS_OVERFLOW) {
            oss << "overflow\n";
        }

        //if (flags & MPFR_FLAGS_DIVBY0) {
            //oss << "divby0\n";
        //}

        if (flags & MPFR_FLAGS_NAN) {
            oss << "nan\n";
        }

        if (flags & MPFR_FLAGS_ERANGE) {
            oss << "erange\n";
        }

        throw std::runtime_error(oss.str());
    }
#endif

    const auto err = mpfr::underflow_flag() || mpfr::overflow_flag() || mpfr::nan_flag() || mpfr::erange_flag();

    if (err) {

        std::ostringstream oss{};
        oss << "error: flags raised in calculation of " << eq << '\n'
            << " at point " << center << '\n'
            << " with radius " << rx << ", " << ry << '\n';

        if (mpfr::underflow_flag()) {
            oss << "underflow\n";
        }

        if (mpfr::overflow_flag()) {
            oss << "overflow\n";
        }

        if (mpfr::nan_flag()) {
            oss << "nan\n";
        }

        if (mpfr::erange_flag()) {
            oss << "erange\n";
        }

        throw std::runtime_error(oss.str());
    }

    return is_pos;
}

template bool Evaluator::is_positive(const EqMap<Sin>& eq, const Coeff64 bx, const Coeff64 by,
                                     const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);
template bool Evaluator::is_positive(const EqMap<Cos>& eq, const Coeff64 bx, const Coeff64 by,
                                     const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);

template <template <typename> typename Trig>
bool Evaluator::is_positive(const EqVec<Trig>& eq, const Coeff64 bound, const PointQ& center, const mpq::Rational& radius) {

    mpfr::clear_flags();

    mpfr::set_zero(sum, 0);

    for (const auto& kv : eq) {

        // t_coeff * trig(x_coeff * center.x * pi/2 + y_coeff * center.y * pi/2)

        const auto x_coeff = kv.first.arg.coeff(XY::X);
        const auto y_coeff = kv.first.arg.coeff(XY::Y);
        const auto t_coeff = kv.second;

        mpq::set(xq, x_coeff); // xq = x_coeff
        mpq::set(yq, y_coeff); // yq = y_coeff

        mpq::mul(xq, xq, center.x); // xq = xq * center.x
        mpq::mul(yq, yq, center.y); // yq = yq * center.y

        mpq::add(argq, xq, yq); // argq = xq + yq

        mpz_fdiv_qr(quot.data(), rem.data(), mpq_numref(argq.data()), mpq_denref(argq.data())); // perform integer division with remainder

        // Reuse xq, yq, argq

        mpq::set(xq, rem);
        mpq_set_z(yq.data(), mpq_denref(argq.data()));

        // This is the remainder fraction. 0 <= argq < 1
        mpq::div(argq, xq, yq);

        // The return value of this function is the remainder
        const auto quad = mpz::fdiv(quot, 4);

        // We can ignore the quotient part of the above remainder, since
        // sin(x + 2pi) = sin(x)
        // cos(x + 2pi) = cos(x)

        eval_trig<Trig>(term, t_coeff, argq, quad, half_pi_d, half_pi_u);

        mpfr::add(sum, sum, term, mpfr::Round::Down);
    }

    // Reuse argq
    mpq::set(argq, bound);
    mpq::mul(argq, argq, radius);

    // Reuse term
    mpfr::mul(term, half_pi_u, argq, mpfr::Round::Up);

    const auto is_pos = sum > term;

    // Now we check for errors.
    // We don't check the INEXACT flag, because that one will be set when evaluating the trig functions
#if 0
    const auto mask = MPFR_FLAGS_UNDERFLOW | MPFR_FLAGS_OVERFLOW | MPFR_FLAGS_NAN | MPFR_FLAGS_ERANGE;

    const auto flags = mpfr_flags_test(mask);

    if (flags != 0) {

        std::ostringstream oss{};
        oss << "error: flags raised in calculation of " << eq
            << " at point " << center
            << " with radius " << radius << '\n';

        if (flags & MPFR_FLAGS_UNDERFLOW) {
            oss << "underflow\n";
        }

        if (flags & MPFR_FLAGS_OVERFLOW) {
            oss << "overflow\n";
        }

        //if (flags & MPFR_FLAGS_DIVBY0) {
            //oss << "divby0\n";
        //}

        if (flags & MPFR_FLAGS_NAN) {
            oss << "nan\n";
        }

        if (flags & MPFR_FLAGS_ERANGE) {
            oss << "erange\n";
        }

        throw std::runtime_error(oss.str());
    }
#endif

    const auto err = mpfr::underflow_flag() || mpfr::overflow_flag() || mpfr::nan_flag() || mpfr::erange_flag();

    if (err) {

        std::ostringstream oss{};
        oss << "error: flags raised in calculation of " << eq << '\n'
            << " at point " << center << '\n'
            << " with radius " << radius << '\n';

        if (mpfr::underflow_flag()) {
            oss << "underflow\n";
        }

        if (mpfr::overflow_flag()) {
            oss << "overflow\n";
        }

        if (mpfr::nan_flag()) {
            oss << "nan\n";
        }

        if (mpfr::erange_flag()) {
            oss << "erange\n";
        }

        throw std::runtime_error(oss.str());
    }

    return is_pos;
}

template bool Evaluator::is_positive(const EqVec<Sin>& eq, const Coeff64 bound,
                                     const PointQ& center, const mpq::Rational& radius);
template bool Evaluator::is_positive(const EqVec<Cos>& eq, const Coeff64 bound,
                                     const PointQ& center, const mpq::Rational& radius);
