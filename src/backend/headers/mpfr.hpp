#pragma once

#include <stdexcept>

#include <mpfr.h>

#include "gmp.hpp"

namespace mpfr {

    using Prec = mpfr_prec_t;

    constexpr Prec prec_min = MPFR_PREC_MIN;
    constexpr Prec prec_max = MPFR_PREC_MAX;

    // mpfr_rnd_t is implemented as an enum, but its values have int as a type
    enum class Round : int {
        Nearest = MPFR_RNDN,
        Zero = MPFR_RNDZ,
        Up = MPFR_RNDU,
        Down = MPFR_RNDD,
        Away = MPFR_RNDA,
        Faithful = MPFR_RNDF,
    };

    class Float {

        private:
        mpfr_t data_;

        public:

        explicit Float(const Prec prec) noexcept {
            mpfr_init2(data_, prec);
        }

        Float(const Float& rhs) noexcept {
            mpfr_init2(data_, mpfr_get_prec(rhs.data()));
            mpfr_set(data_, rhs.data(), MPFR_RNDN);
        }

        Float(Float&& rhs) noexcept {
            mpfr_init2(data_, MPFR_PREC_MIN);   // should be an "empty" object, and this is the closest thing
            mpfr_swap(data_, rhs.data());
        }

        void operator=(Float&& rhs) noexcept {
            mpfr_swap(data_, rhs.data());
        }

        void operator=(const Float& rhs) noexcept {
            mpfr_set_prec(data_, mpfr_get_prec(rhs.data()));
            mpfr_set(data_, rhs.data(), MPFR_RNDN);
        }

        ~Float() {
            mpfr_clear(data_);
        }

        mpfr_t& data() {
            return data_;
        }

        const mpfr_t& data() const {
            return data_;
        }
    };

    // ------------------------------------------------------------------------
    // Assignment
    // ------------------------------------------------------------------------

    inline void set(Float& flt, const long rhs, const Round rnd) {
        mpfr_set_si(flt.data(), rhs, static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    inline void set_prec(Float& flt, const Prec prec) noexcept {
        mpfr_set_prec(flt.data(), prec);
    }

    inline Prec get_prec(const Float& flt) noexcept {
        return mpfr_get_prec(flt.data());
    }

    inline void set_nan(Float& flt) noexcept {
        mpfr_set_nan(flt.data());
    }

    inline void set_inf(Float& flt, const int sign) noexcept {
        mpfr_set_inf(flt.data(), sign);
    }

    inline void set_zero(Float& flt, const int sign) noexcept {
        mpfr_set_zero(flt.data(), sign);
    }

    // ------------------------------------------------------------------------
    // Arithmetic
    // ------------------------------------------------------------------------

    inline int add(Float& res, const Float& lhs, const Float& rhs, const Round rnd) noexcept {
        return mpfr_add(res.data(), lhs.data(), rhs.data(), static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    inline int mul(Float& res, const Float& lhs, const Float& rhs, const Round rnd) noexcept {
        return mpfr_mul(res.data(), lhs.data(), rhs.data(), static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    inline int mul(Float& res, const Float& lhs, const mpq::Rational& rhs, const Round rnd) noexcept {
        return mpfr_mul_q(res.data(), lhs.data(), rhs.data(), static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    inline int mul(Float& res, const Float& lhs, const long rhs, const Round rnd) noexcept {
        return mpfr_mul_si(res.data(), lhs.data(), rhs, static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    inline int div(Float& res, const Float& lhs, const unsigned long rhs, const Round rnd) noexcept {
        return mpfr_div_ui(res.data(), lhs.data(), rhs, static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    // ------------------------------------------------------------------------
    // Special
    // ------------------------------------------------------------------------

    inline int cos(Float& res, const Float& arg, const Round rnd) noexcept {
        return mpfr_cos(res.data(), arg.data(), static_cast<mpfr_rnd_t>(static_cast<mpfr_rnd_t>(rnd)));
    }

    inline int sin(Float& res, const Float& arg, const Round rnd) noexcept {
        return mpfr_sin(res.data(), arg.data(), static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    inline int tan(Float& res, const Float& arg, const Round rnd) noexcept {
        return mpfr_tan(res.data(), arg.data(), static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    inline int const_pi(Float& res, const Round rnd) noexcept {
        return mpfr_const_pi(res.data(), static_cast<mpfr_rnd_t>(static_cast<int>(rnd)));
    }

    // ------------------------------------------------------------------------
    // Comparison
    // ------------------------------------------------------------------------

    inline bool operator==(const Float& lhs, const Float& rhs) noexcept {
        return mpfr_equal_p(lhs.data(), rhs.data());
    }

    inline bool operator!=(const Float& lhs, const Float& rhs) noexcept {
        return !(lhs == rhs);
    }

    inline bool operator<(const Float& lhs, const Float& rhs) noexcept {
        return mpfr_less_p(lhs.data(), rhs.data());
    }

    inline bool operator>(const Float& lhs, const Float& rhs) noexcept {
        return mpfr_greater_p(lhs.data(), rhs.data());
    }

    inline bool operator<=(const Float& lhs, const Float& rhs) noexcept {
        return mpfr_lessequal_p(lhs.data(), rhs.data());
    }

    inline bool operator>=(const Float& lhs, const Float& rhs) noexcept {
        return mpfr_greaterequal_p(lhs.data(), rhs.data());
    }

    inline bool is_nan(const Float& flt) noexcept {
        return mpfr_nan_p(flt.data());
    }

    inline bool is_inf(const Float& flt) noexcept {
        return mpfr_inf_p(flt.data());
    }

    inline int sgn(const Float& flt) noexcept {
        return mpfr_sgn(flt.data());
    }

    // ------------------------------------------------------------------------
    // Exception Handling
    // ------------------------------------------------------------------------

    inline void clear_underflow_flag() noexcept {
        mpfr_clear_underflow();
    }

    inline void clear_overflow_flag() noexcept {
        mpfr_clear_overflow();
    }

    inline void clear_divby0_flag() noexcept {
        mpfr_clear_divby0();
    }

    inline void clear_flags() noexcept {
        mpfr_clear_flags();
    }

    inline bool underflow_flag() noexcept {
        return mpfr_underflow_p();
    }

    inline bool overflow_flag() noexcept {
        return mpfr_overflow_p();
    }

    inline bool divby0_flag() noexcept {
        return mpfr_divby0_p();
    }

    inline bool nan_flag() noexcept {
        return mpfr_nanflag_p();
    }

    inline bool inexact_flag() noexcept {
        return mpfr_inexflag_p();
    }

    inline bool erange_flag() noexcept {
        return mpfr_erangeflag_p();
    }

#if 0
    // need to define bitset operations on this enum
    enum class Flags : mpfr_flags_t {
        Underflow = MPFR_FLAGS_UNDERFLOW,
        Overflow = MPFR_FLAGS_OVERFLOW,
        DivBy0 = MPFR_FLAGS_DIVBY0,
        NaN = MPFR_FLAGS_NAN,
        Inexact = MPFR_FLAGS_INEXACT,
        Erange = MPFR_FLAGS_ERANGE,
        All = MPFR_FLAGS_ALL,
    };

    // TODO put these in a flags namespace?

    inline void flags_clear(const Flags& mask) noexcept {
        mpfr_flags_clear(static_cast<mpfr_mask_t>(mask));
    }

    inline void flags_set(const Flags& mask) noexcept {
        mpfr_flags_set(static_cast<mpfr_mask_t>(mask));
    }

    inline Flags flags_test(const Flags& mask) noexcept {
        const auto flags = mpfr_flags_test(static_cast<mpfr_flags_t>(mask));
        return static_cast<Flags>(flags);
    }

    inline Flags flags_save() noexcept {
        const auto flags = mpfr_flags_save();
        return static_cast<Flags>(flags);
    }

    inline void flags_restore(const Flags& flags, const Flags& mask) noexcept {
        mpfr_flags_restore(static_cast<mpfr_flags_t>(flags), static_cast<mpfr_flags_t>(mask));
    }
#endif

    // ------------------------------------------------------------------------
    // Memory Management
    // ------------------------------------------------------------------------

    inline void free_cache() noexcept {
        mpfr_free_cache();
    }
}
