This project has two components: the C++ backend that analyzes and verifies the cover, and a Java GUI that allows inspecting the cover visually.

This code was written, developed, and tested on Linux and Mac. The C++ backend should compile on any Linux distribution. The Java GUI is written in the JavaFX framework, which should work on any distribution that packages Java 14.

Note: This program was tested and run on Ubuntu 18.04 and Mac OS 10.14. The C++ and Java code require recent compilers, and may not work on older systems.

## Dependencies

On Linux, the dependencies can be installed using the system package manager. Here we give instructions for Ubuntu, but other distributions will be similar.

```
$ sudo apt install build-essential git openjdk-14-jdk libgmp-dev libmpfr-dev libtbb-dev
```

On a Mac, one first needs to install the Command Line Developer Tools, which contain a C++ compiler and various other utilities. In the terminal, run `xcode-select --install` and select the Install button. Next, we need to install the [Homebrew](http://brew.sh) package manager. Once you have followed the installation instructions on the website, the dependencies can be installed as follows.

```
$ brew install git openjdk gmp mpfr tbb
$ sudo ln -sfn /usr/local/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk
```

## Compiling

Next, we need to clone the repository and compile the code.

```
$ git clone https://gitlab.com/jwgarber/billiard-viewer.git
$ cd billiard-viewer
$ ./gradlew run
```

This will clone the repository using git, download the Java dependencies, compile the C++ and Java code, and run the GUI.

Alternatively, the C++ cover verifier can be run independently of the Java program. This can be done using the meson build system.

```
$ sudo apt install meson # Ubuntu
$ brew install meson     # Mac
```

Then, to setup meson

```
$ cd billiard-viewer
$ meson meson
$ ninja -C meson cover
$ meson/cover coversfolder/105cover     # check the 105cover, for example
```

