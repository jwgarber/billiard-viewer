#pragma once

#include <array>
#include <limits>
#include <map>
#include <ostream>
#include <set>
#include <sstream>
#include <vector>

// Unfortunately, we need this, since it is possible to cast an
// integer to an enum value outside the class
// Switches are ok, if you are returning from each branch. If
// you are not, use an if/else.
template <typename T>
std::string invalid_enum_value(const std::string& enum_name, const T value) {
    std::ostringstream err{};
    err << "unknown " << enum_name << " value " << static_cast<size_t>(value);
    return err.str();
}

// Return -1 if lhs < rhs
// Return 0 if lhs == rhs
// Return 1 if lhs > rhs
template <typename T>
int compare(const T lhs, const T rhs) {
    return (lhs > rhs) - (lhs < rhs);
}

// Intended for used with signed integers
template <typename Int>
Int signum(const Int val) {
    return compare(val, 0);
}

template <typename T, typename S>
std::ostream& operator<<(std::ostream& os, const std::pair<T, S>& p) {
    return os << '(' << p.first << ", " << p.second << ')';
}

// Why does this not already exist?
template <typename Cont>
std::ostream& print_container(std::ostream& os, const Cont& cont) {

    os << '[';

    bool first = true;
    for (const auto& elem : cont) {
        if (!first) {
            os << ", ";
        }

        first = false;

        os << elem;
    }

    os << ']';
    return os;
}

template <typename T, size_t N>
std::ostream& operator<<(std::ostream& os, const std::array<T, N>& arr) {
    return print_container(os, arr);
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec) {
    return print_container(os, vec);
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& set) {
    return print_container(os, set);
}

template <typename T, typename S>
std::ostream& operator<<(std::ostream& os, const std::map<T, S>& map) {
    return print_container(os, map);
}

inline bool is_digit(const char c) {
    // The standard guarantees that all digits are contiguous characters between '0' and '9'
    return ('0' <= c) && (c <= '9');
}

template <typename T>
typename std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value, T>::type
parse_int(const std::string& str) {

    if (str.empty()) {
        throw std::runtime_error("parse_int: cannot parse empty string");
    }

    auto it = std::begin(str);

    // Skip the positive sign if there is one
    if (*it == '+') {
        ++it;

        if (it == std::end(str)) {
            throw std::runtime_error("parse_int: cannot parse \"+\"");
        }
    }

    constexpr T max = std::numeric_limits<T>::max();

    T num = 0;
    while (it != std::end(str)) {

        const auto c = *it;

        if (!is_digit(c)) {
            throw std::runtime_error("parse_int: unable to parse \"" + str + "\": non-digit character(s)");
        }

        const T digit = c - '0';

        // 10 * num > max
        if (num > max / 10) {
            throw std::runtime_error("parse_int: unable to parse \"" + str + "\": out of range");
        }

        num *= 10;

        // num + digit > max
        if (num > max - digit) {
            throw std::runtime_error("parse_int: unable to parse \"" + str + "\": out of range");
        }

        num += digit;
        ++it;
    }

    return num;
}

template <typename T>
typename std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value, T>::type
parse_int(const std::string& str) {

    if (str.empty()) {
        throw std::runtime_error("parse_int: cannot parse empty string");
    }

    auto it = std::begin(str);

    if (*it == '-') {
        // negative number

        // Skip the negative sign
        ++it;

        if (it == std::end(str)) {
            throw std::runtime_error("parse_int: cannot parse \"-\"");
        }

        constexpr T min = std::numeric_limits<T>::min();

        T num = 0;
        while (it != std::end(str)) {

            const auto c = *it;

            if (!is_digit(c)) {
                throw std::runtime_error("parse_int: unable to parse \"" + str + "\": non-digit character(s)");
            }

            const T digit = c - '0';

            // 10 * num < min
            // Division of a negative number rounds towards 0, so this is fine
            if (num < min / 10) {
                throw std::runtime_error("parse_int: unable to parse \"" + str + "\": out of range");
            }

            num *= 10;

            // num - digit < min
            if (num < min + digit) {
                throw std::runtime_error("parse_int: unable to parse \"" + str + "\": out of range");
            }

            num -= digit;
            ++it;
        }

        return num;

    } else {
        // positive number

        // Skip the positive sign if there is one
        if (*it == '+') {
            ++it;

            if (it == std::end(str)) {
                throw std::runtime_error("parse_int: cannot parse \"+\"");
            }
        }

        constexpr T max = std::numeric_limits<T>::max();

        T num = 0;
        while (it != std::end(str)) {

            const auto c = *it;

            if (!is_digit(c)) {
                throw std::runtime_error("parse_int: unable to parse \"" + str + "\": non-digit character(s)");
            }

            const T digit = c - '0';

            // 10 * num > max
            if (num > max / 10) {
                throw std::runtime_error("parse_int: unable to parse \"" + str + "\": out of range");
            }

            num *= 10;

            // num + digit > max
            if (num > max - digit) {
                throw std::runtime_error("parse_int: unable to parse \"" + str + "\": out of range");
            }

            num += digit;
            ++it;
        }

        return num;
    }
}

std::string replace_copy(std::string str, const std::string& search, const std::string& replace);

std::vector<std::string> split(const std::string& str, const char delim);

void trim(std::string& str);

std::string read_file(const std::string& path);
