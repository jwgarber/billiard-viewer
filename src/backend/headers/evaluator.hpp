#pragma once

#include "general.hpp"
#include "mpfr.hpp"

class Evaluator {
  private:
    mpz::Integer quot;
    mpz::Integer rem;

    mpq::Rational xq;
    mpq::Rational yq;
    mpq::Rational argq;  // In theory we can remove this variable, but it is very helpful for clarity

    mpfr::Float term;
    mpfr::Float sum;

    mpfr::Float half_pi_d;
    mpfr::Float half_pi_u;

  public:
    explicit Evaluator(const mpfr::Prec prec) :
        term{prec}, sum{prec},
        // These are important constants, so they have twice the precision
        half_pi_d{2 * prec}, half_pi_u{2 * prec} {

            mpfr::const_pi(half_pi_d, mpfr::Round::Down);
            mpfr::const_pi(half_pi_u, mpfr::Round::Up);

            mpfr::div(half_pi_d, half_pi_d, 2, mpfr::Round::Down);
            mpfr::div(half_pi_u, half_pi_u, 2, mpfr::Round::Up);
        }

    template <template <typename> typename Trig>
    bool is_positive(const EqVec<Trig>& eq, const Coeff64 bound,
                     const PointQ& center, const mpq::Rational& radius);

    template <template <typename> typename Trig>
    bool is_positive(const EqVec<Trig>& eq, const Coeff64 bx, const Coeff64 by,
                     const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);

    template <template <typename> typename Trig>
    bool is_positive(const EqMap<Trig>& eq, const Coeff64 bx, const Coeff64 by,
                     const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);
};

extern template bool Evaluator::is_positive(const EqVec<Sin>& eq, const Coeff64 bound,
                                            const PointQ& center, const mpq::Rational& radius);
extern template bool Evaluator::is_positive(const EqVec<Cos>& eq, const Coeff64 bound,
                                            const PointQ& center, const mpq::Rational& radius);

extern template bool Evaluator::is_positive(const EqVec<Sin>& eq, const Coeff64 bx, const Coeff64 by,
                                            const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);
extern template bool Evaluator::is_positive(const EqVec<Cos>& eq, const Coeff64 bx, const Coeff64 by,
                                            const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);

extern template bool Evaluator::is_positive(const EqMap<Sin>& eq, const Coeff64 bx, const Coeff64 by,
                                            const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);
extern template bool Evaluator::is_positive(const EqMap<Cos>& eq, const Coeff64 bx, const Coeff64 by,
                                            const PointQ& center, const mpq::Rational& rx, const mpq::Rational& ry);
