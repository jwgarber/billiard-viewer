#pragma once

#include <ostream>   // std::ostream
#include <stdexcept> // std::runtime_error

#include "../gmp.hpp"
#include "topology.hpp"

namespace geometry {

// A proper bounded interval
template <Topology Top>
class Interval final {

  private:
    // These are the lower and upper endpoints of the interval
    mpq::Rational lower_;
    mpq::Rational upper_;

    // Degeneracies are not allowed, so lower_ < upper_
    void check() const {

        if (lower_ >= upper_) {
            throw std::runtime_error("degenerate Interval: lower >= upper");
        }
    }

  public:
    Interval(mpq::Rational lower, mpq::Rational upper)
        : lower_{std::move(lower)}, upper_{std::move(upper)} {
        check();
    }

    const mpq::Rational& lower() const {
        return lower_;
    }

    const mpq::Rational& upper() const {
        return upper_;
    }

    mpq::Rational length() const {
        // return upper_ - lower_;

        mpq::Rational l;
        mpq::sub(l, upper_, lower_);
        return l;
    }

    mpq::Rational center() const {

        // return (lower_ + upper_) / 2;

        mpq::Rational c;
        mpq::add(c, lower_, upper_);
        mpq::div_2exp(c, c, 1);

        return c;
    }

    friend bool operator==(const Interval<Top>& lhs, const Interval<Top>& rhs) {
        return std::tie(lhs.lower_, lhs.upper_) == std::tie(rhs.lower_, rhs.upper_);
    }

    friend bool operator!=(const Interval<Top>& lhs, const Interval<Top>& rhs) {
        return !(lhs == rhs);
    }

    friend bool operator<(const Interval<Top>& lhs, const Interval<Top>& rhs) {
        return std::tie(lhs.lower_, lhs.upper_) < std::tie(rhs.lower_, rhs.upper_);
    }

    friend bool operator>(const Interval<Top>& lhs, const Interval<Top>& rhs) {
        return rhs < lhs;
    }

    friend bool operator<=(const Interval<Top>& lhs, const Interval<Top>& rhs) {
        return !(lhs > rhs);
    }

    friend bool operator>=(const Interval<Top>& lhs, const Interval<Top>& rhs) {
        return rhs <= lhs;
    }
};

inline std::ostream& operator<<(std::ostream& os, const Interval<Topology::Closed>& interval) {
    return os << '[' << interval.lower() << ", " << interval.upper() << ']';
}

inline std::ostream& operator<<(std::ostream& os, const Interval<Topology::Open>& interval) {
    return os << ']' << interval.lower() << ", " << interval.upper() << '[';
}
}
