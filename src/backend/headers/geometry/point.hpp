#pragma once

#include <ostream>   // std::ostream
#include <stdexcept> // std::runtime_error
#include <tuple>     // std::tie

#include "gmp.hpp"

namespace geometry {

class Point final {

  public:
    mpq::Rational x;
    mpq::Rational y;

    Point() {}

    Point(mpq::Rational x_, mpq::Rational y_)
        : x{std::move(x_)}, y{std::move(y_)} {}

    /*
    void operator+=(const Point& point) {
        x += point.x;
        y += point.y;
    }

    void operator-=(const Point& point) {
        x -= point.x;
        y -= point.y;
    }

    void operator*=(const N& scale) {
        x *= scale;
        y *= scale;
    }

    void operator/=(const N& scale) {

        if (scale == 0) {
            throw std::runtime_error("division by zero in Point::operator/=");
        }

        x /= scale;
        y /= scale;
    }

    // These could go outside, but I think it is nicer if it is inside
    friend Point operator+(Point lhs, const Point& rhs) {
        lhs += rhs;
        return lhs;
    }

    friend Point operator-(Point lhs, const Point& rhs) {
        lhs -= rhs;
        return lhs;
    }

    friend Point operator*(Point point, const N& scale) {
        point *= scale;
        return point;
    }

    friend Point operator/(Point point, const N& scale) {

        if (scale == 0) {
            throw std::runtime_error("division by zero in Point::operator/");
        }

        point /= scale;
        return point;
    }
    */

    friend bool operator==(const Point& lhs, const Point& rhs) {
        return std::tie(lhs.x, lhs.y) == std::tie(rhs.x, rhs.y);
    }

    friend bool operator!=(const Point& lhs, const Point& rhs) {
        return !(lhs == rhs);
    }

    friend bool operator<(const Point& lhs, const Point& rhs) {
        return std::tie(lhs.x, lhs.y) < std::tie(rhs.x, rhs.y);
    }

    friend bool operator>(const Point& lhs, const Point& rhs) {
        return rhs < lhs;
    }

    friend bool operator<=(const Point& lhs, const Point& rhs) {
        return !(lhs > rhs);
    }

    friend bool operator>=(const Point& lhs, const Point& rhs) {
        return rhs <= lhs;
    }

    friend std::ostream& operator<<(std::ostream& os, const Point& point) {
        return os << '(' << point.x << ", " << point.y << ')';
    }

    static mpq::Rational dot(const Point& u, const Point& v) {

        // return u.x * v.x + u.y * v.y;

        mpq::Rational a;
        mpq::Rational b;

        mpq::mul(a, u.x, v.x);
        mpq::mul(b, u.y, v.y);
        mpq::add(a, a, b);

        return a;
    }

    // z component of u x v, where u and v are extended to three dimensions
    static mpq::Rational cross(const Point& u, const Point& v) {

        // return u.x * v.y - u.y * v.x;

        mpq::Rational a;
        mpq::Rational b;

        mpq::mul(a, u.x, v.y);
        mpq::mul(b, u.y, v.x);
        mpq::sub(a, a, b);

        return a;
    }

    // Given three points u, v, w, this function returns their orientation.
    // (Note that this is equal to twice the signed area of the triangle uvw)
    // This is the shoelace formula for the special case of a triangle
    // The result is
    // +1 for CCW
    //  0 for collinear
    // -1 for CW
    static int orientation(const Point& u, const Point& v, const Point& w) {

        // a = (v.x - u.x) * (w.y - u.y);
        // b = (w.x - u.x) * (v.y - u.y);
        // order = a.compare(b);

        mpq::Rational a;
        mpq::Rational b;
        mpq::Rational c;

        mpq::sub(a, v.x, u.x);
        mpq::sub(c, w.y, u.y);
        mpq::mul(a, a, c);

        mpq::sub(b, w.x, u.x);
        mpq::sub(c, v.y, u.y);
        mpq::mul(b, b, c);

        return mpq::cmp(a, b);

        // Get the signum
        //return (order > 0) - (order < 0);
    }
};
}
