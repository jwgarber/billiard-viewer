#pragma once

#include <algorithm> // std::minmax
#include <variant>   // std::variant

#include "convex_polygon.hpp"
#include "point.hpp"
#include "rectangle.hpp"
#include "segment.hpp"

namespace geometry {

// ------------------------------------------------------------------------
// Segment
// ------------------------------------------------------------------------

// These could return a point or an interval
template <Topology Top>
std::variant<mpq::Rational, Interval<Top>> project_impl(const mpq::Rational& first, const mpq::Rational& second) {

    const auto order = mpq::cmp(first, second);

    if (order < 0) {
        return Interval<Top>{first, second};
    }

    if (order > 0) {
        return Interval<Top>{second, first};
    }

    // Else they are equal, so just return the point
    return first;
}

template <Topology Top>
std::variant<mpq::Rational, Interval<Top>> project_x(const Segment<Top>& seg) {

    return project_impl<Top>(seg.start().x, seg.end().x);
}

template <Topology Top>
std::variant<mpq::Rational, Interval<Top>> project_y(const Segment<Top>& seg) {

    return project_impl<Top>(seg.start().y, seg.end().y);
}

template <Topology Top>
std::variant<mpq::Rational, Interval<Top>> project(const Segment<Top>& seg, const Point& axis) {

    const auto start_dot = Point::dot(seg.start(), axis);
    const auto end_dot = Point::dot(seg.end(), axis);

    return project_impl<Top>(start_dot, end_dot);
}

// ------------------------------------------------------------------------
// Rectangle
// ------------------------------------------------------------------------

// Rectangles are always non-degenerate, so projection will always return intervals,
// never points.

template <Topology Top>
Interval<Top> project_x(const Rectangle<Top>& rect) {
    return rect.interval_x;
}

template <Topology Top>
Interval<Top> project_y(const Rectangle<Top>& rect) {
    return rect.interval_y;
}

template <Topology Top>
Interval<Top> project(const Rectangle<Top>& rect, const Point& axis) {

    const auto ll_dot = Point::dot(rect.lower_left(), axis);

    const auto ul_dot = Point::dot(rect.upper_left(), axis);

    const auto ur_dot = Point::dot(rect.upper_right(), axis);

    const auto lr_dot = Point::dot(rect.lower_right(), axis);

    const auto minmax = std::minmax({ll_dot, ul_dot, ur_dot, lr_dot});

    return {minmax.first, minmax.second};
}

// ------------------------------------------------------------------------
// Convex Polygon
// ------------------------------------------------------------------------

// Polygons are also always non-degenerate, so they always return intervals,
// never points.

// Find the min and max x values of all the points
template <Topology Top>
Interval<Top> project_x(const ConvexPolygon<Top>& poly) {

    const auto& first = poly.vertex(0);

    // Note: the polygon is normalized, so the first point always has the smallest
    // x coordinate.
    const auto min = first.x;
    auto max = first.x;

    for (size_t i = 1; i < poly.size(); ++i) {

        const auto& point = poly.vertex(i);

        if (point.x > max) {
            max = point.x;
        }
    }

    return {min, max};
}

// Find the min and max y values of all the points
template <Topology Top>
Interval<Top> project_y(const ConvexPolygon<Top>& poly) {

    const auto& first = poly.vertex(0);

    auto min = first.y;
    auto max = first.y;

    for (size_t i = 1; i < poly.size(); ++i) {

        const auto& point = poly.vertex(i);

        // We know min <= max, so we only need to do the following comparisons

        if (point.y < min) {
            min = point.y;
        } else if (point.y > max) {
            max = point.y;
        }

        // else min <= y <= max, so there are no changes to make
    }

    return {min, max};
}

template <Topology Top>
Interval<Top> project(const ConvexPolygon<Top>& poly, const Point& axis) {

    const auto& first = poly.vertex(0);

    auto dot = Point::dot(first, axis);

    auto min = dot;
    auto max = dot;

    for (size_t i = 1; i < poly.size(); ++i) {

        const auto& point = poly.vertex(i);

        dot = Point::dot(point, axis);

        // We know min <= max, so we only need to do the following comparisons

        if (dot < min) {
            min = dot;
        } else if (dot > max) {
            max = dot;
        }

        // else min <= dot <= max, so there are no changes to make
    }

    return {min, max};
}
}
