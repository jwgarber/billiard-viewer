#pragma once

#include <ostream> // std::ostream

#include "interval.hpp"
#include "point.hpp"

namespace geometry {

template <Topology Top>
class Rectangle final {

  private:
    Interval<Top> interval_x_; // x-axis
    Interval<Top> interval_y_; // y-axis

  public:
    // All necessary checking is done within the interval classes
    Rectangle(Interval<Top> x, Interval<Top> y)
        : interval_x_{std::move(x)}, interval_y_{std::move(y)} {}

    const Interval<Top>& interval_x() const {
        return interval_x_;
    }

    const Interval<Top>& interval_y() const {
        return interval_y_;
    }

    Point lower_left() const {
        return {interval_x_.lower(), interval_y_.lower()};
    }

    Point lower_right() const {
        return {interval_x_.upper(), interval_y_.lower()};
    }

    Point upper_left() const {
        return {interval_x_.lower(), interval_y_.upper()};
    }

    Point upper_right() const {
        return {interval_x_.upper(), interval_y_.upper()};
    }

    mpq::Rational width() const {
        return interval_x_.length();
    }

    mpq::Rational height() const {
        return interval_y_.length();
    }

    Point center() const {
        return {interval_x_.center(), interval_y_.center()};
    }

    bool is_square() const {
        return width() == height();
    }

    friend bool operator==(const Rectangle<Top>& lhs, const Rectangle<Top>& rhs) {
        return std::tie(lhs.interval_x_, lhs.interval_y_) == std::tie(rhs.interval_x_, rhs.interval_y_);
    }

    friend bool operator!=(const Rectangle<Top>& lhs, const Rectangle<Top>& rhs) {
        return !(lhs == rhs);
    }

    friend bool operator<(const Rectangle<Top>& lhs, const Rectangle<Top>& rhs) {
        return std::tie(lhs.interval_x_, lhs.interval_y_) < std::tie(rhs.interval_x_, rhs.interval_y_);
    }

    friend bool operator>(const Rectangle<Top>& lhs, const Rectangle<Top>& rhs) {
        return rhs < lhs;
    }

    friend bool operator<=(const Rectangle<Top>& lhs, const Rectangle<Top>& rhs) {
        return !(lhs > rhs);
    }

    friend bool operator>=(const Rectangle<Top>& lhs, const Rectangle<Top>& rhs) {
        return rhs <= lhs;
    }

    friend std::ostream& operator<<(std::ostream& os, const Rectangle<Top>& rect) {
        return os << '(' << rect.interval_x_ << ", " << rect.interval_y_ << ')';
    }
};
}
