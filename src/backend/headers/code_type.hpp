#pragma once

#include <ostream>

enum class CodeType {
    OSO, // odd stable oblique
    ESP, // even stable perp
    ESO, // even stable oblique
    EUP, // even unstable perp
    EUO, // even unstable oblique
};

std::ostream& operator<<(std::ostream& os, const CodeType code_type);

bool is_stable(const CodeType code_type);
