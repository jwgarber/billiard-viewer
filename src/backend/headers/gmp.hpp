#pragma once

#include <stdexcept>
#include <ostream>

#include <gmp.h>

namespace mp {
    using BitCount = mp_bitcnt_t;
}

namespace mpz {

    class Integer {

        private:
        mpz_t data_;

        public:

        Integer() noexcept {
            mpz_init(data_);
        }

        Integer(const Integer& rhs) noexcept {
            mpz_init_set(data_, rhs.data());
        }

        Integer(Integer&& rhs) noexcept {
            mpz_init(data_);
            mpz_swap(data_, rhs.data());
        }

        void operator=(Integer&& rhs) noexcept {
            mpz_swap(data_, rhs.data());
        }

        void operator=(const Integer& rhs) noexcept {
            mpz_set(data_, rhs.data());
        }

        ~Integer() {
            mpz_clear(data_);
        }

        mpz_t& data() {
            return data_;
        }

        const mpz_t& data() const {
            return data_;
        }

        // Extra constructors
        // These stay inside, becaues they are init_set functions
        // that naturally map to constructors
        //explicit Integer(const unsigned long rhs) noexcept {
            //mpz_init_set_ui(data_, rhs);
        //}

        explicit Integer(const long rhs) noexcept {
            mpz_init_set_si(data_, rhs);
        }

        explicit Integer(const char* const str, const int base = 10) {

            const auto rval = mpz_init_set_str(data_, str, base);

            if (rval != 0) {
                mpz_clear(data_);
                const std::string msg = "unable to convert to Integer: ";
                throw std::runtime_error(msg + str);
            }
        }

        explicit Integer(const std::string& str, const int base = 10) : Integer(str.c_str(), base) {}

    };

    // ------------------------------------------------------------------------
    // Assignment
    // ------------------------------------------------------------------------
    //inline void set(Integer& res, unsigned long rhs) noexcept {
        //mpz_set_ui(res.data(), rhs);
    //}

    inline void set(Integer& res, long rhs) noexcept {
        mpz_set_si(res.data(), rhs);
    }

    // Not having = for these is much better. Eg, it allows us to set the base.
    inline void set(Integer& res, const char* const str, const int base = 10) {

        const auto rval = mpz_set_str(res.data(), str, base);

        if (rval != 0) {
            const std::string msg = "unable to convert to Integer: ";
            throw std::runtime_error(msg + str);
        }
    }

    inline void set(Integer& res, const std::string& str, const int base = 10) {
        set(res, str.c_str(), base);
    }

    // ------------------------------------------------------------------------
    // Arithmetic
    // ------------------------------------------------------------------------

    inline void add(Integer& res, const Integer& lhs, const Integer& rhs) noexcept {
        mpz_add(res.data(), lhs.data(), rhs.data());
    }

    inline void sub(Integer& res, const Integer& lhs, const Integer& rhs) noexcept {
        mpz_sub(res.data(), lhs.data(), rhs.data());
    }

    inline void mul(Integer& res, const Integer& lhs, const Integer& rhs) noexcept {
        mpz_mul(res.data(), lhs.data(), rhs.data());
    }

    // ------------------------------------------------------------------------
    // Comparison
    // ------------------------------------------------------------------------

    inline int cmp(const Integer& lhs, const Integer& rhs) noexcept {
        return mpz_cmp(lhs.data(), rhs.data());
    }

    inline int cmp(const Integer& lhs, const long rhs) noexcept {
        return mpz_cmp_si(lhs.data(), rhs);
    }

    inline int cmp(const Integer& arg) noexcept {
        return mpz_sgn(arg.data());
    }

    // ------------------------------------------------------------------------
    // Integer Division
    // ------------------------------------------------------------------------

    inline unsigned long fdiv(const Integer& n, const unsigned long d) {
        return mpz_fdiv_ui(n.data(), d);
    }
}

namespace mpq {

    // As an invariant, a Rational is always in canonical form
    class Rational {

        private:
        mpq_t data_;

        public:
        Rational() noexcept {
            mpq_init(data_);
        }

        Rational(const Rational& rhs) noexcept {
            mpq_init(data_);
            mpq_set(data_, rhs.data());
        }

        Rational(const long num) noexcept {
            mpq_init(data_);
            mpq_set_si(data_, num, 1);
        }

        Rational(const long num, const unsigned long den) {

            if (den == 0) {
                throw std::runtime_error("mpq::Rational: division by zero");
            }

            // This must only be initialized after the exception is thrown, since
            // destructors won't be run in a constructor.
            mpq_init(data_);
            mpq_set_si(data_, num, den);
            mpq_canonicalize(data_);
        }

        void operator=(const Rational& rhs) noexcept {
            mpq_set(data_, rhs.data());
        }

        Rational(Rational&& rhs) noexcept {
            mpq_init(data_);
            mpq_swap(data_, rhs.data());
        }

        void operator=(Rational&& rhs) noexcept {
            mpq_swap(data_, rhs.data());
        }

        ~Rational() {
            mpq_clear(data_);
        }

        mpq_t& data() {
            return data_;
        }

        const mpq_t& data() const {
            return data_;
        }
    };

    inline std::ostream& operator<<(std::ostream& os, const Rational& rat) {

        char* str = mpq_get_str(nullptr, 10, rat.data());

        os << str;

        free(str);

        return os;
    }

    // ------------------------------------------------------------------------
    // Assignment
    // ------------------------------------------------------------------------

    inline void set(Rational& lhs, const mpz::Integer& rhs) noexcept {
        mpq_set_z(lhs.data(), rhs.data());
    }

    //inline void set(Rational& res, const unsigned long num) noexcept {
        //mpq_set_ui(res.data(), num, 1);
    //}

    inline void set(Rational& res, const long num) noexcept {
        mpq_set_si(res.data(), num, 1);
    }

    //inline void set(Rational& res, const unsigned long num, const unsigned long den) {

        //if (den == 0) {
            //throw std::runtime_error("mpq::set: division by zero");
        //}

        //mpq_set_ui(res.data(), num, den);
        //mpq_canonicalize(res.data());
    //}

    inline void set(Rational& res, const long num, const unsigned long den) {

        if (den == 0) {
            throw std::runtime_error("mpq::set: division by zero");
        }

        mpq_set_si(res.data(), num, den);
        mpq_canonicalize(res.data());
    }

    inline void set(Rational& res, const char* const str, const int base = 10) {

        const auto rval = mpq_set_str(res.data(), str, base);

        if (rval != 0) {
            std::string msg = "mpq::set: unable to convert to Rational: ";
            throw std::runtime_error(msg + str);
        }

        mpq_canonicalize(res.data());
    }

    inline void set(Rational& res, const std::string& str, const int base = 10) {
        set(res, str.c_str(), base);
    }

    // ------------------------------------------------------------------------
    // Arithmetic
    // ------------------------------------------------------------------------

    inline void add(Rational& res, const Rational& lhs, const Rational& rhs) noexcept {
        mpq_add(res.data(), lhs.data(), rhs.data());
    }

    inline void sub(Rational& res, const Rational& lhs, const Rational& rhs) noexcept {
        mpq_sub(res.data(), lhs.data(), rhs.data());
    }

    inline void mul(Rational& res, const Rational& lhs, const Rational& rhs) noexcept {
        mpq_mul(res.data(), lhs.data(), rhs.data());
    }

    inline void div(Rational& res, const Rational& lhs, const Rational& rhs) {

        if (mpq_sgn(rhs.data()) == 0) {
            throw std::runtime_error("mpq::div: division by zero");
        }

        mpq_div(res.data(), lhs.data(), rhs.data());
    }

    inline void mul_2exp(Rational& res, const Rational& lhs, const mp::BitCount bits) noexcept {
        mpq_mul_2exp(res.data(), lhs.data(), bits);
    }

    inline void div_2exp(Rational& res, const Rational& lhs, const mp::BitCount bits) noexcept {
        mpq_div_2exp(res.data(), lhs.data(), bits);
    }

    inline void neg(Rational& res, const Rational& rhs) noexcept {
        mpq_neg(res.data(), rhs.data());
    }

    inline void abs(Rational& res, const Rational& rhs) noexcept {
        mpq_abs(res.data(), rhs.data());
    }

    inline void inv(Rational& res, const Rational& rhs) {

        if (mpq_sgn(rhs.data()) == 0) {
            throw std::runtime_error("mpq::inv: division by zero");
        }

        mpq_inv(res.data(), rhs.data());
    }

    // ------------------------------------------------------------------------
    // Comparison
    // ------------------------------------------------------------------------

    inline int cmp(const Rational& lhs, const Rational& rhs) noexcept {
        return mpq_cmp(lhs.data(), rhs.data());
    }

    inline int cmp(const Rational& lhs, const mpz::Integer& rhs) noexcept {
        return mpq_cmp_z(lhs.data(), rhs.data());
    }

    //inline int cmp(const Rational& lhs, const unsigned long num) noexcept {
        //return mpq_cmp_ui(lhs.data(), num, 1);
    //}

    inline int cmp(const Rational& lhs, const long num) noexcept {
        return mpq_cmp_si(lhs.data(), num, 1);
    }

    //inline int cmp(const Rational& lhs, const unsigned long num, const unsigned long den) {

        //if (den == 0) {
            //throw std::runtime_error("mpq::cmp: division by zero");
        //}

        //return mpq_cmp_ui(lhs.data(), num, den);
    //}

    inline int cmp(const Rational& lhs, const long num, const unsigned long den) {

        if (den == 0) {
            throw std::runtime_error("mpq::cmp: division by zero");
        }

        return mpq_cmp_si(lhs.data(), num, den);
    }

    inline int sgn(const Rational& rat) noexcept {
        return mpq_sgn(rat.data());
    }

    inline bool operator==(const Rational& lhs, const Rational& rhs) noexcept {
        return mpq_equal(lhs.data(), rhs.data());
    }

    inline bool operator!=(const Rational& lhs, const Rational& rhs) noexcept {
        return !(lhs == rhs);
    }

    inline bool operator<(const Rational& lhs, const Rational& rhs) noexcept {
        return mpq_cmp(lhs.data(), rhs.data()) < 0;
    }

    inline bool operator>(const Rational& lhs, const Rational& rhs) noexcept {
        return rhs < lhs;
    }

    inline bool operator<=(const Rational& lhs, const Rational& rhs) noexcept {
        return !(lhs > rhs);
    }

    inline bool operator>=(const Rational& lhs, const Rational& rhs) noexcept {
        return rhs <= lhs;
    }
}

