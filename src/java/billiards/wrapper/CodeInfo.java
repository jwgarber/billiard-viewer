package billiards.wrapper;

import billiards.geometry.Point;
import billiards.math.CosEquation;
import billiards.math.SinEquation;

import com.google.common.base.Splitter;

import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.List;

public final class CodeInfo {

    public final ImmutableList<Point> points;
    public final ImmutableList<SinEquation> sinEquations;
    public final ImmutableList<CosEquation> cosEquations;

    public CodeInfo(final String points, final String sinEquations, final String cosEquations) {
        this.points = parsePoints(points);
        this.sinEquations = parseSinEquations(sinEquations);
        this.cosEquations = parseCosEquations(cosEquations);
    }

    private static ImmutableList<SinEquation> parseSinEquations(final String string) {

        final Iterable<String> strings = Splitter.on('\n').split(string);

        final MutableList<SinEquation> equations = new FastList<>();

        for (final String str : strings) {

            final List<String> stringCoeffs = Splitter.on(' ').splitToList(str);

            final int[] coeffs = new int[stringCoeffs.size()];
            for (int i = 0; i < stringCoeffs.size(); ++i) {
                coeffs[i] = Integer.parseInt(stringCoeffs.get(i));
            }

            final SinEquation eq = new SinEquation(coeffs);

            equations.add(eq);
        }

        return equations.toImmutable();
    }

    private static ImmutableList<CosEquation> parseCosEquations(final String string) {

        final Iterable<String> strings = Splitter.on('\n').split(string);

        final MutableList<CosEquation> equations = new FastList<>();

        for (final String str : strings) {

            final List<String> stringCoeffs = Splitter.on(' ').splitToList(str);

            final int[] coeffs = new int[stringCoeffs.size()];
            for (int i = 0; i < stringCoeffs.size(); ++i) {
                coeffs[i] = Integer.parseInt(stringCoeffs.get(i));
            }

            final CosEquation eq = new CosEquation(coeffs);

            equations.add(eq);
        }

        return equations.toImmutable();
    }

    private static ImmutableList<Point> parsePoints(final String string) {

        final Iterable<String> strings = Splitter.on('\n').split(string);

        final MutableList<Point> points = new FastList<>();

        for (final String str : strings) {

            final List<String> split = Splitter.on(' ').splitToList(str);

            final double x = rationalToRadians(split.get(0));
            final double y = rationalToRadians(split.get(1));

            final Point point = Point.create(x, y);
            points.add(point);
        }

        return points.toImmutable();
    }

    public static double rationalToRadians(final String rat) {
        if (rat.indexOf('/') == -1) {
            // Does not contain /, so an integer
            final double num = Integer.parseInt(rat);
            return num * Math.PI / 2.0;
        } else {
            final List<String> nums = Splitter.on('/').splitToList(rat);
            final double numer = Integer.parseInt(nums.get(0));
            final double denom = Integer.parseInt(nums.get(1));

            return numer / denom * Math.PI / 2.0;
        }
    }

}
